from pony.orm import Required, Set

import cryptowallet.model  # import Coin
import cryptowallet.tools.database


class User(cryptowallet.tools.database.db.Entity):
    currency = Required(str)
    name = Required(str)
    coins = Set("Coin")
