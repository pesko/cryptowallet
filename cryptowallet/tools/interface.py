from typing import Any, Dict

import cryptowallet.model as model
import cryptowallet.tools.operations as op


def profit(results: list[float], coin: str, currency: str) -> str:
    return f"""
    | | | total profit for your {coin}: {op.get_wallet_profit(results)} {currency}
    """


def add_coin(coin: model.Coin) -> str:
    result = f"""
        | name: {coin.name}
        | value: {coin.value}
        | entry: {coin.entrydate}
    """
    return result


def wallet(result: Dict[str, Any]) -> str:
    res = ""
    total_profit = []
    for d in result:
        total_profit.append(d["profit"])
        res += f"""
        | id: {d["id"]}
        | name: {d["coin"]}
        | value: {d["value"]}
        | profit: {round(d["profit"], 2)} {d["currency"]}
        | entry: {d["entrydate"]}
        """
    res += f"""
    | | | total profit: {op.get_wallet_profit(total_profit)} {d["currency"]}
    """
    return res
