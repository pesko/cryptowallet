import os
from pathlib import Path

from pony.orm import Database, db_session  # noqa: 401

db = Database()


def init_database():
    conf_path = f"{str(Path.home())}/.config/cryptowallet"
    if not os.path.exists(conf_path):
        os.makedirs(name=conf_path)
    db.bind(
        create_db=True,
        filename=f"{conf_path}/cryptowallet_db.sqlite",
        provider="sqlite",
    )
    db.generate_mapping(create_tables=True)
