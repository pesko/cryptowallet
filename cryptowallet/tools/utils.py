import datetime


def date_is_valid(date) -> bool:
    return datetime.datetime.strptime(date, "%d-%m-%Y")


def value_is_valid(value) -> bool:
    if isinstance(value, (int, float)):
        return True
