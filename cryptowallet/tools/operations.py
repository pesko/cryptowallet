import cryptowallet.services.coingecko as cg


def get_history_value(coin_id: str, currency: str, date: str, value: float) -> float:
    """Calcul the value at the origin."""
    return value * cg.get_history_price(coin_id, date, currency)


def get_current_value(coin_id: str, currency: str, value: float) -> float:
    """Calcul the value at the origin."""
    return value * cg.get_current_price(coin_id, currency)


def get_profit_for_coin(coin_id: str, currency: str, date: str, value: float) -> float:
    """Calcul the profit for specific coin."""
    return get_current_value(coin_id, currency, value) - get_history_value(
        coin_id, currency, date, value
    )


def get_wallet_profit(profits: list[float]) -> float:
    """Return total profit for your wallet."""
    return round(sum(profits), 2)
