import cryptowallet.model as model
import cryptowallet.services.coingecko as cg
import cryptowallet.tools.database as db


@db.db_session
def get(username: str) -> model.User:
    return model.User.get(name=username)


@db.db_session
def create(currency: str, username: str) -> None:
    if get(username):
        raise ValueError("User already exist")
    cg.currency_is_valid(currency)
    model.User(currency=currency, name=username)


@db.db_session
def update(currency: str) -> None:
    user = get("default")
    cg.currency_is_valid(currency)
    user.currency = currency
