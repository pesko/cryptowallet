import datetime
from typing import Dict

from pycoingecko import CoinGeckoAPI

cg = CoinGeckoAPI()


def get_supported_currencies() -> list[str]:
    return cg.get_supported_vs_currencies()


def currency_is_valid(currency: str) -> bool:
    if currency in get_supported_currencies():
        return True
    else:
        raise ValueError(currency, "This currency is not valid.")


def get_coins() -> list[Dict[str, str]]:
    return cg.get_coins_list()


def coin_is_valid(coin: str) -> bool:
    if coin in {x["id"] for x in cg.get_coins()}:
        return True
    else:
        raise ValueError(coin, "This coin is not valid.")


def get_current_price(coin_id: str, currency: str) -> float:
    """Get the current price."""
    return cg.get_coin_history_by_id(
        id=coin_id, date=datetime.datetime.now().strftime("%d-%m-%Y")
    )["market_data"]["current_price"][currency]


def get_history_price(coin_id: str, date: str, currency: str) -> float:
    """Get the coin price at specific date.

    :param coin_id:
    :param date: DAY-MONTH-YEAR ex: 25-06-1988
    """
    return cg.get_coin_history_by_id(id=coin_id, date=date)["market_data"][
        "current_price"
    ][currency]
